package com.epam.view;

import com.epam.model.PizzaType;
import com.epam.model.PriceCalculator;
import com.epam.model.bakery.Bakery;
import com.epam.model.bakery.impl.Kyiv;
import com.epam.model.bakery.impl.Lviv;
import com.epam.model.bakery.impl.Odessa;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class View {
    public static final Logger logger = LogManager.getLogger(View.class);
    private static Scanner scanner = new Scanner(System.in);
    private String wrong = "Wrong input, choose again.";
    private boolean execute = true;
    private int input;
    private PriceCalculator price = new PriceCalculator();

    public void print() {
        while (execute) {
            chooseAction();
        }
    }

    private void chooseAction() {
        logger.info("Please, select menu point:\n" +
                "1 - Order pizza\n" +
                "0 - exit");
        input = scanner.nextInt();
        if (input == 1) {
            Bakery bakery = chooseBakery();
            bakery.assemble(choosePizza());
        } else if (input == 0) {
            execute = false;
        } else {
            logger.info(wrong);
            chooseAction();
        }
    }

    private Bakery chooseBakery() {
        logger.info("Chose bakery: \n" +
                "1 Kyiv \n" +
                "2 Lviv \n" +
                "3 Odessa");
        input = scanner.nextInt();
        Bakery bakery = null;
        if (input == 1) {
            bakery = new Kyiv();
        } else if (input == 2) {
            bakery = new Lviv();
        } else if (input == 3) {
            bakery = new Odessa();
        } else {
            logger.info(wrong);
            return chooseBakery();
        }
        return bakery;
    }

    private PizzaType choosePizza() {
        logger.info("Chose pizza: \n" +
                "1 Greek \n" +
                "2 Hawaiian \n" +
                "3 Neapolitan\n" +
                "4 Pepperoni");
        PizzaType type = null;
        input = scanner.nextInt();
        if (input == 1) {
            type = PizzaType.Greek;
            logger.info("Price: " + price.calculate(PizzaType.Greek));
        } else if (input == 2) {
            type = PizzaType.Hawaiian;
            logger.info("Price: " + price.calculate(PizzaType.Hawaiian));
        } else if (input == 3) {
            type = PizzaType.Neapolitan;
            logger.info("Price: " + price.calculate(PizzaType.Neapolitan));
        } else if (input == 4) {
            type = PizzaType.Pepperoni;
            logger.info("Price: " + price.calculate(PizzaType.Pepperoni));
        } else {
            logger.info(wrong);
            return choosePizza();
        }
        return type;
    }
}
