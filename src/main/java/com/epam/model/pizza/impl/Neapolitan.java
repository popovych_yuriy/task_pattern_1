package com.epam.model.pizza.impl;

import com.epam.model.pizza.Pizza;
import com.epam.view.View;

public class Neapolitan implements Pizza {
    @Override
    public void prepare() {
        View.logger.info("Neapolitan pizza preparing.");
    }

    @Override
    public void bake() {
        View.logger.info("Neapolitan pizza baking.");
    }

    @Override
    public void cut() {
        View.logger.info("Neapolitan pizza cutting.");
    }

    @Override
    public void box() {
        View.logger.info("Neapolitan pizza boxing.");
    }
}
