package com.epam.model.pizza.impl;

import com.epam.model.pizza.Pizza;
import com.epam.view.View;

public class Hawaiian implements Pizza {
    @Override
    public void prepare() {
        View.logger.info("Hawaiian pizza preparing.");
    }

    @Override
    public void bake() {
        View.logger.info("Hawaiian pizza baking.");
    }

    @Override
    public void cut() {
        View.logger.info("Hawaiian pizza cutting.");
    }

    @Override
    public void box() {
        View.logger.info("Hawaiian pizza boxing.");
    }
}
