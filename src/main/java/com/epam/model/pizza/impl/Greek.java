package com.epam.model.pizza.impl;

import com.epam.model.pizza.Pizza;
import com.epam.view.View;

public class Greek implements Pizza {
    @Override
    public void prepare() {
        View.logger.info("Greek pizza preparing.");
    }

    @Override
    public void bake() {
        View.logger.info("Greek pizza baking.");
    }

    @Override
    public void cut() {
        View.logger.info("Greek pizza cutting.");
    }

    @Override
    public void box() {
        View.logger.info("Greek pizza boxing.");
    }
}
