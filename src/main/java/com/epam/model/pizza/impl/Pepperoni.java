package com.epam.model.pizza.impl;

import com.epam.model.pizza.Pizza;
import com.epam.view.View;

public class Pepperoni implements Pizza {
    @Override
    public void prepare() {
        View.logger.info("Pepperoni pizza preparing.");
    }

    @Override
    public void bake() {
        View.logger.info("Pepperoni pizza baking.");
    }

    @Override
    public void cut() {
        View.logger.info("Pepperoni pizza cutting.");
    }

    @Override
    public void box() {
        View.logger.info("Pepperoni pizza boxing.");
    }
}
