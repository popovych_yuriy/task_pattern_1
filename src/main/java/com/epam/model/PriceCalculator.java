package com.epam.model;

import com.epam.model.components.Component;
import com.epam.model.receipt.*;
import com.epam.view.View;

public class PriceCalculator {
    Receipt receipt;

    public int calculate(PizzaType type) {
        int price = 0;

        if (type == PizzaType.Greek) {
            receipt = new GreekReceipt();
            View.logger.info("Components of pizza: ");
            for (Component component : receipt.getReceipt()) {
                View.logger.info(component.getName() + " " + component.getPrice() / 100);
                price += component.getPrice() / 100;
            }
        } else if (type == PizzaType.Hawaiian) {
            receipt = new HawaiianReceipt();
            View.logger.info("Components of pizza: ");
            for (Component component : receipt.getReceipt()) {
                View.logger.info(component.getName() + " " + component.getPrice() / 100);
                price += component.getPrice() / 100;
            }
        } else if (type == PizzaType.Neapolitan) {
            receipt = new NeapolitanReceipt();
            View.logger.info("Components of pizza: ");
            for (Component component : receipt.getReceipt()) {
                View.logger.info(component.getName() + " " + component.getPrice() / 100);
                price += component.getPrice() / 100;
            }
        } else {
            receipt = new PepperoniReceipt();
            View.logger.info("Components of pizza: ");
            for (Component component : receipt.getReceipt()) {
//                View.logger.info(component.getName() +" " + component.getPrice()/100);
                price += component.getPrice() / 100;
            }
        }
        return price;
    }
}
