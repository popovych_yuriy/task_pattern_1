package com.epam.model;

public enum PizzaType {
    Pepperoni, Neapolitan, Hawaiian, Greek
}
