package com.epam.model.components;

import java.util.ArrayList;
import java.util.List;

public class Toppings implements Components {
    private List<Component> toppings = new ArrayList<>();

    public Toppings() {
        toppings.add(new Component("Pineapple", 2000));
        toppings.add(new Component("Sausage", 2500));
        toppings.add(new Component("Bacon", 2000));
        toppings.add(new Component("Ham", 2500));
        toppings.add(new Component("Mushrooms", 1000));
        toppings.add(new Component("Onion", 1000));
        toppings.add(new Component("Extra cheese", 1000));
        toppings.add(new Component("Black olives", 1000));
        toppings.add(new Component("Green peppers", 1000));
        toppings.add(new Component("Red peppers", 1000));
        toppings.add(new Component("Pineapple", 1000));
        toppings.add(new Component("Spinach", 1000));
        toppings.add(new Component("Feta cheese", 2500));
        toppings.add(new Component("Mozzarella cheese", 2500));
    }

    public List<Component> getList() {
        return toppings;
    }
}
