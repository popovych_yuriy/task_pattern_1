package com.epam.model.components;

import java.util.List;

public interface Components {
    List<Component> getList();
}
