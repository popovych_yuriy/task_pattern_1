package com.epam.model.components;

import java.util.ArrayList;
import java.util.List;

public class Dough implements Components {
    private List<Component> dough = new ArrayList<>();

    public Dough() {
        dough.add(new Component("Thin", 8000));
        dough.add(new Component("Thick", 9000));
    }

    public List<Component> getList() {
        return dough;
    }
}
