package com.epam.model.bakery.impl;

import com.epam.model.PizzaType;
import com.epam.model.bakery.Bakery;
import com.epam.model.pizza.Pizza;
import com.epam.model.pizza.impl.Greek;
import com.epam.model.pizza.impl.Hawaiian;
import com.epam.model.pizza.impl.Neapolitan;
import com.epam.model.pizza.impl.Pepperoni;

public class Kyiv extends Bakery {
    @Override
    protected Pizza createPizza(PizzaType pizzaType) {
        Pizza pizza = null;

        if (pizzaType == PizzaType.Pepperoni) {
            pizza = new Pepperoni();
        } else if (pizzaType == PizzaType.Greek) {
            pizza = new Greek();
        } else if (pizzaType == PizzaType.Hawaiian) {
            pizza = new Hawaiian();
        } else {
            pizza = new Neapolitan();
        }
        return pizza;
    }
}
