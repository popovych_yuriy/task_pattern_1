package com.epam.model.bakery;

import com.epam.model.PizzaType;
import com.epam.model.pizza.Pizza;

public abstract class Bakery {
    protected abstract Pizza createPizza(PizzaType pizzaType);

    public Pizza assemble(PizzaType pizzaType) {
        Pizza pizza = createPizza(pizzaType);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
}
