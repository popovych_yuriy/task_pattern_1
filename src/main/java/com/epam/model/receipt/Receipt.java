package com.epam.model.receipt;

import com.epam.model.components.Component;

import java.util.List;

public interface Receipt {
    List<Component> getReceipt();
}
