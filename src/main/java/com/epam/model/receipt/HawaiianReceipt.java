package com.epam.model.receipt;

import com.epam.model.components.Component;
import com.epam.model.components.Dough;
import com.epam.model.components.Toppings;

import java.util.ArrayList;
import java.util.List;

public class HawaiianReceipt implements Receipt {
    private List<Component> receipt = new ArrayList<>();
    private List<Component> doughs = new Dough().getList();
    private List<Component> toppings = new Toppings().getList();

    public HawaiianReceipt() {
        setReceipt();
    }

    private void setReceipt() {
        for (Component dough : doughs) {
            if (dough.getName() == "Thin") {
                receipt.add(dough);
            }
        }
        for (Component topping : toppings) {
            if (topping.getName() == "Pineapple") {
                receipt.add(topping);
            } else if (topping.getName() == "Ham") {
                receipt.add(topping);
            } else if (topping.getName() == "Feta cheese") {
                receipt.add(topping);
            } else if (topping.getName() == "Bacon") {
                receipt.add(topping);
            }
        }
    }

    public List<Component> getReceipt() {
        return receipt;
    }
}
