package com.epam.model.receipt;

import com.epam.model.components.Component;
import com.epam.model.components.Dough;
import com.epam.model.components.Toppings;

import java.util.ArrayList;
import java.util.List;

public class PepperoniReceipt implements Receipt {
    private List<Component> receipt = new ArrayList<>();
    private List<Component> doughs = new Dough().getList();
    private List<Component> toppings = new Toppings().getList();

    public PepperoniReceipt() {
        setReceipt();
    }

    private void setReceipt() {
        for (Component dough : doughs) {
            if (dough.getName() == "Thick") {
                receipt.add(dough);
            }
        }
        for (Component topping : toppings) {
            if (topping.getName() == "Papperoni") {
                receipt.add(topping);
            } else if (topping.getName() == "Onion") {
                receipt.add(topping);
            } else if (topping.getName() == "Bacon") {
                receipt.add(topping);
            } else if (topping.getName() == "Black olives") {
                receipt.add(topping);
            } else if (topping.getName() == "Feta cheese") {
                receipt.add(topping);
            }
        }
    }

    public List<Component> getReceipt() {
        return receipt;
    }
}
